from quixote import get_publisher

from wcs.qommon import _
from wcs.qommon.publisher import get_publisher_class, get_request
from wcs.qommon.misc import get_cfg

from modules import payments
from modules import connectors

get_publisher_class().register_translation_domain('auquotidien')
get_publisher_class().default_configuration_path = 'au-quotidien-wcs-settings.xml'
