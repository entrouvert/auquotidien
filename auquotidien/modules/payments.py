from wcs.qommon import _, N_
from wcs.qommon.storage import StorableObject

from wcs.workflows import EvolutionPart, WorkflowStatusItem, register_item_class


class Regie(StorableObject):
    _names = 'regies'


class Invoice(StorableObject):
    _names = 'invoices'


class InvoiceEvolutionPart(EvolutionPart):
    pass


class Transaction(StorableObject):
    _names = 'transactions'


class PaymentWorkflowStatusItem(WorkflowStatusItem):
    description = N_('Payment Creation')
    key = 'payment'
    endpoint = False
    category = 'interaction'

    @classmethod
    def is_available(self, workflow=None):
        return False

    def render_as_line(self):
        return _('Payable (obsolete)')

    def get_parameters(self):
        return ()

    def perform(self, formdata):
        pass


register_item_class(PaymentWorkflowStatusItem)


class PaymentCancelWorkflowStatusItem(WorkflowStatusItem):
    description = N_('Payment Cancel')
    key = 'payment-cancel'
    endpoint = False
    category = 'interaction'

    @classmethod
    def is_available(self, workflow=None):
        return False

    def render_as_line(self):
        return _('Cancel Payments (obsolete)')

    def get_parameters(self):
        return ()

    def perform(self, formdata):
        pass


register_item_class(PaymentCancelWorkflowStatusItem)


class PaymentValidationWorkflowStatusItem(WorkflowStatusItem):
    description = N_('Payment Validation')
    key = 'payment-validation'
    endpoint = False
    category = 'interaction'

    @classmethod
    def is_available(self, workflow=None):
        return False

    def render_as_line(self):
        return _('Wait for payment validation (obsolete)')

    def get_parameters(self):
        return ()

    def perform(self, formdata):
        pass


register_item_class(PaymentValidationWorkflowStatusItem)
