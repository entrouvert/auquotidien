from wcs.workflows import WorkflowStatusItem


class AbeliumDominoRegisterFamilyWorkflowStatusItem(WorkflowStatusItem):
    key = 'abelium-domino-register-family'
