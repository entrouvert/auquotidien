import os
import shutil
import time

import pytest

from quixote import cleanup, get_publisher
from wcs.qommon import errors, sessions
from wcs.qommon.ident.password_accounts import PasswordAccount
from wcs.qommon.http_request import HTTPRequest
from wcs.categories import Category
from wcs.workflows import Workflow
from wcs.formdef import FormDef
from wcs import fields

from utilities import get_app, login, create_temporary_pub, clean_temporary_pub


def setup_module(module):
    cleanup()

    global pub

    pub = create_temporary_pub()

    req = HTTPRequest(None, {'SCRIPT_NAME': '/', 'SERVER_NAME': 'example.net'})
    pub.set_app_dir(req)
    pub.cfg['identification'] = {'methods': ['password']}
    pub.write_cfg()


def teardown_module(module):
    clean_temporary_pub()


def create_user():
    if pub.user_class.get_users_with_name_identifier('user'):
        return pub.user_class.get_users_with_name_identifier('user')[0]
    user1 = pub.user_class(name='user')
    user1.name_identifiers = ['user']
    user1.is_admin = False
    user1.store()

    account1 = PasswordAccount(id='user')
    account1.set_password('user')
    account1.user_id = user1.id
    account1.store()

    pub.cfg['identification'] = {'methods': ['password']}
    pub.write_cfg()

    return user1


def create_formdef():
    FormDef.wipe()
    formdef = FormDef()
    formdef.name = 'test'
    formdef.fields = []
    formdef.store()
    return formdef


def teardown_module(module):
    shutil.rmtree(pub.APP_DIR)


def test_with_user():
    user = create_user()
    app = login(get_app(pub), username='user', password='user')
    resp = app.get('/', status=200)


def test_form_category_redirection():
    Category.wipe()
    cat = Category(name='baz')
    cat.store()

    FormDef.wipe()
    formdef = FormDef()
    formdef.name = 'foobar'
    formdef.category_id = cat.id
    formdef.fields = []
    formdef.store()

    # check we get a redirection to /category/formdef/
    resp = get_app(pub).get('/foobar/')
    assert resp.location.endswith('/baz/foobar/')

    # check missing trailing slashs are ok
    resp = get_app(pub).get('/foobar')
    assert resp.location.endswith('/foobar/')
    resp = resp.follow()
    assert resp.location.endswith('/baz/foobar/')

    # check direct category access without trailing slash
    resp = get_app(pub).get('/baz')
    assert resp.location.endswith('/baz/')
