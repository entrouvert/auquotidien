import os
import shutil
import time

try:
    import lasso
except ImportError:
    lasso = None

import pytest

from quixote import cleanup, get_publisher
from wcs.qommon import errors, sessions
from wcs.qommon.ident.password_accounts import PasswordAccount
from wcs.qommon.http_request import HTTPRequest
from wcs.categories import Category
from wcs.roles import Role
from wcs.workflows import Workflow
from wcs.formdef import FormDef
from wcs import fields

from utilities import get_app, login, create_temporary_pub, clean_temporary_pub


def setup_module(module):
    cleanup()

    global pub

    pub = create_temporary_pub()

    req = HTTPRequest(None, {'SCRIPT_NAME': '/', 'SERVER_NAME': 'example.net'})
    pub.set_app_dir(req)
    pub.cfg['identification'] = {'methods': ['password']}
    pub.write_cfg()


def teardown_module(module):
    clean_temporary_pub()


def create_superuser():
    global user1
    if pub.user_class.get_users_with_name_identifier('admin'):
        user1 = pub.user_class.get_users_with_name_identifier('admin')[0]
        user1.is_admin = True
        user1.roles = []
        return
    user1 = pub.user_class(name='admin')
    user1.is_admin = True
    user1.name_identifiers = ['admin']
    user1.roles = []
    user1.store()

    account1 = PasswordAccount(id='admin')
    account1.set_password('admin')
    account1.user_id = user1.id
    account1.store()

    pub.cfg['identification'] = {'methods': ['password']}
    pub.write_cfg()


def create_role():
    Role.wipe()
    role = Role(name='foobar')
    role.store()
    return role


def teardown_module(module):
    shutil.rmtree(pub.APP_DIR)


@pytest.fixture
def empty_siteoptions():
    open(os.path.join(pub.app_dir, 'site-options.cfg'), 'w').close()


def test_with_superuser():
    create_superuser()
    app = login(get_app(pub))
    # this makes sure the extension loaded properly
    assert 'auquotidien' in pub.translation_domains
