#! /bin/bash

set -e

for DIRECTORY in "htmlcov" "venv"
do
    if [ -d "$DIRECTORY" ]; then
        rm -r $DIRECTORY
    fi
done

test -d wcs || git clone git://repos.entrouvert.org/wcs.git
(cd wcs && git pull)
(cd wcs && git clean -xdf)

virtualenv -p /usr/bin/python3 --system-site-packages venv
PIP_BIN=venv/bin/pip

rm -f coverage.xml
rm -f test_results.xml
cat << _EOF_ > .coveragerc
[run]
omit = wcs/qommon/vendor/*.py

[report]
omit = wcs/qommon/vendor/*.py
_EOF_

$PIP_BIN install --upgrade 'pip!=20.3.2'
$PIP_BIN install --upgrade setuptools
$PIP_BIN install --upgrade 'pytest' 'attrs' 'more-itertools' WebTest mock pytest-cov
$PIP_BIN install --upgrade 'pylint'
$PIP_BIN install --upgrade 'Quixote>=3.0,<3.2' 'Django>=2.2,<2.3' 'gadjo' 'vobject' 'pyproj' 'requests' 'django-ratelimit<3' 'bleach' 'unidecode' 'pyquery' 'lxml' 'phonenumbers'
$PIP_BIN install git+https://git.entrouvert.org/debian/django-ckeditor.git

DJANGO_SETTINGS_MODULE=wcs.settings \
WCS_SETTINGS_FILE=tests/settings.py \
LC_ALL=C LC_TIME=C LANG=C  PYTHONPATH=$(pwd)/wcs/:$PYTHONPATH venv/bin/py.test --junitxml=test_results.xml --cov-report xml --cov-report html --cov=auquotidien/ --cov-config .coveragerc tests/
test -f pylint.out && cp pylint.out pylint.out.prev
(PYTHONPATH=$(pwd)/wcs/:$(pwd)/wcs/wcs/:$PYTHONPATH venv/bin/pylint -f parseable --rcfile /var/lib/jenkins/pylint.wcs.rc --ignore=pyatom.py auquotidien/modules/ | tee pylint.out) || /bin/true
test -f pylint.out.prev && (diff pylint.out.prev pylint.out | grep '^[><]' | grep .py) || /bin/true
