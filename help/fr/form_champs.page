<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="form_champs">

<info>
	<title  type="sort">2</title>
	<link type="guide" xref="index#formulaires" />
	<desc>Éditer le contenu d'un formulaire</desc>
</info>

<title>Ajouter des champs dans un formulaire</title>

<section id="creation">
	
	<title>Création des champs</title>

         <p>Pour créer le contenu de votre formulaire, vous devez y ajouter des champs. Ce sont les questions auxquelles devront répondre les usagers d'une part, des éléments de mise en forme (<link xref="form_champs#champ_affiche_info">Nouvelle page, Titre, Sous-titre, Commentaire</link>) d'autre part.</p>
        
        <p>Sur la page d'édition du formulaire que vous venez de créer ou que vous souhaitez éditer, dans la partie <em>Champs</em> vous avez un lien <em>éditer</em> :</p>
        
        
        <figure>
            <media type="image" src="figures/form_champ_editer.png" />
        </figure>
        
        <p>En cliquant dessus vous êtes redirigé sur la page de création des champs. Vous avez sur cette page une section <em>Nouveau champ</em> :</p>
        
        <figure>
            <media type="image" src="figures/page_ajout_champ.png" />
        </figure>
        
        <p>
            Le <em>Libellé</em> est le texte qui sera affiché à l'utilisateur et <em>Type</em> propose une liste de <link xref="types_champs">différents types de champ</link>. Les types disponibles permettent au concepteur du formulaire de créer une information texte, une date, de télécharger un fichier, de choisir une réponse dans une liste, de lui afficher une information ou encore de créer plusieurs pages, etc.</p>
        
        <p>Pour créer un champ il faut obligatoirement saisir son libellé et choisir son type, puis, il faut cliquer sur <em>Ajouter</em>. Une fois créé le champ apparaît sur la page :</p>
        
        <figure>
            <media type="image" src="figures/liste_champ.png" />
        </figure>
        
        <p>Les champs sont affichés les uns en dessous des autres. Pour chacun, le libellé et le type sont affichés.</p>
        
        <p>Pour éditer un champ placez votre souris sur celui qui vous intéresse, un petit menu apparaît :</p>
        
        <figure>
            <media type="image" src="figures/form_champ_menu_survol_souris.png" />
        </figure>
        
        <p>Vous pouvez supprimer le champ, le dupliquer ou l'éditer.</p>
        
        <p>Pour chacun, la page d'édition vous proposera diverses options à paramétrer.</p>

</section>

<section id="options_champs">

	<title>Options des champs</title>

	<p>Chaque champ peut-être édité en cliquant sur le lien éditer qui apparaît au moment du survol.</p>
	<p>Il existe deux types de champs, ceux qui demandent à l'usager d'entrer une information et ceux qui lui donnent une information (les champs de mise en forme). À l'exception des champs de mise en forme (<link xref="form_champs#champ_affiche_info">Nouvelle page, Titre, Sous-titre, Commentaire</link>), tous offrent les options suivantes :</p>

	<list>
 		<item><p>Libellé : le texte du champs tel qu'il sera présenté à l'utilisateur.</p></item>
		<item><p>Obligatoire : il s'agit d'une case à cocher qui fixe le caractère obligatoire d'une réponse pour le répondant, si la case n'est pas cochée, la réponse à la question est optionnelle.</p></item>
		<item><p>Remarque : permet d'apporter une aide au répondant. Dans le cas d'un champ adresse électronique, on pourra faire figurer dans cette option : "par exemple : francis.kuntz@wanagro.com", et ce message sera affiché à l'utilisateur en-dessous du champ adresse électronique.</p></item>
		<item><p>Affichage dans les listings : il n'est pas forcément pertinent que tous les champs figurent dans le listing de back-office (optimisation de la lisibilité). Cette option permet donc, par le biais d'une case à cocher de déterminer si le champ concerné doit figurer ou non dans ce listing.</p></item>
	 	<item><p>Classe supplémentaire pour les styles CSS : permet d'indiquer une classe CSS pour le champ. Cette classe doit évidemment exister dans la feuille de style du thème choisi pour être opérante.</p></item>
		<item><p>Nom de variable : permet d'attribuer un identifiant au champ en question. Cet identifiant pourra ensuite être utilisé pour faire des <link xref="form_conditionnel">formulaires conditionnels</link>.</p></item>
	</list>

</section>
        
<section id="types_champs">
            <title>Les différents types de champs</title>
                      
            <section id="champ_demande_info">
                <title>Champs demandant une information</title>
                
                <p>Ces champs exigent que l'utilisateur entre du texte, une date, choisissent une ou plusieurs réponse(s), joigne un fichier...</p>

		<list>
		  <item><p>Texte : C'est le champ de base, utilisé pour demander au répondant de fournir une information en texte libre. Le champ texte permet, dans sa configuration de base, une réponse comprenant des lettres et des chiffres et limitée à 20 caractères. Tout ceci peut être modifié aisément en éditant le champs. En plus des options communes, il offre :</p>
			<list>
			  <item><p>Longueur de ligne : permet d'indiquer la largeur du champs (par défaut 20).</p></item>
			  <item><p>Regex de validation : ce champ permet d'appliquer une règle (en respectant la sysntace des expressions régulière Python) pour contrôler ce qui est saisi par l'utilisateur. Exemple de règles :</p>
				<list>
				  <item><p>Imposer la saisie d'un numéro de téléphone à 10 chiffres : \d{10}$</p></item>
				  <item><p>Idem mais avec la possibilité d'avoir des espaces entre les groupes de 2 chiffres : \d{2}\D*\d{2}\D*\d{2}\D*\d{2}\D*\d{2}$</p></item>
				  <item><p>Code postal à 5 chiffres : \d{5}$</p></item>
				  <item><p>Valeur numérique : \d+$</p></item>
				</list>			
			</item>
			  <item><p>La liste déroulante « Préremplir », permet de pré-remplir de 3 façons différentes le champ :</p>
				<list>
				  <item><p>« Texte », qui en cliquant sur le bouton « appliquer » vous permettra de saisir une valeur fixe qui sera affichée indifféremment à tous les répondants.</p></item>
				  <item><p>« Champ utilisateur » qui permet de sélectionner une des caractéristiques du profil de l'utilisateur (nom, adresse téléphone...) pour alimenter le champs</p></item>
				  <item><p>« Formule Python » qui permet d'écrire une requête Python pour aller récupérer une information pouvant pré-remplir le champ.</p></item>
				</list></item>
			  <item><p>Source de données externe : permet d'aller interroger un référentiel distant pour en extraire une information destinée à alimenter le formulaire. La source de données peut être de type webservice formattée en JSON.</p></item>
			</list>
		</item>
		  <item><p>Texte long : identique au champ Texte mais il s'étend sur plusieurs lignes. Dans les navigateurs récents, la taille du bloc de texte est modifiable par l'utilisateur en cliquant/déplaçant son coin inférieur droit. Par défaut le nombre de caractères par ligne est fixé à 20 et le nombre de lignes à 3. Pour un affichage occupant la largeur de la page, 70 caractères par ligne est une bonne valeur. 10 lignes par bloc permettent une réponse longue, sachant que si l'utilisateur dépasse, il disposera d'une barre d'ascenseur. L'option « texte préformaté » Permet de conserver les sauts à la lignes et les espaces supérieur à 1 caractère dans la réponse de l'usager.</p></item>
		  <item><p>Courriel : C'est un champ Texte sur lequel un contrôle est opéré pour vérifier que c'est bien une adresse électronique valable qui est saisie. Le répondant devra donc mentionner dans ce champ une adresse de courriel valide. Le contrôle porte à la fois sur la forme de l'adresse et sur la validité du nom de domaine.</p></item>
		  <item><p>Case à cocher : il s'agit d'une case à cocher permettant par exemple d'obtenir le consentement du demandeur.</p></item>
		  <item><p>Upload de fichier : permet à l'utilisateur de joindre un fichier au formulaire, par exemple une pièce justificative scannée.</p></item>
		  <item><p>Date : Propose à l'utilisateur de choisir une date (en l'écrivant ou en la sélectionnant dans un calendrier). Ce champs offre les options suivantes :</p>
			<list>
			  <item><p>Date minimale : empêche l'utilisateur de choisir une date
                    antérieure à celle définie ici.</p></item>
                    		
			  		<item><p>La date doit être dans le futur : impose à l'utilisateur de choisir une date postérieure à la date du jour. Cette option n'est pas compatible avec l'option <em>Date minimale</em>.</p></item>
			  		<item><p>Date maximale : empêche l'utilisateur de choisir une date postérieure à la date définie ici.</p></item>
			  		<item><p>La date doit être dans le passé : impose à l'utilisateur de choisir une date antérieure à la date du jour. Cette option n'est pas compatible avec l'option <em>Date maximale</em>.</p></item>
			  		<item><p>La date peut être le jour présent : option qui n'est utile que combinée aux options précédentes et qui leur ajoute la possibilité de saisir la date du jour.</p></item>
			</list>
		  </item>
		  <item><p>Liste : Ce champ permet l'affichage d'une liste déroulante au répondant. « Éléments », permet de définir les différentes possibilités de réponse. Ajoutez autant d'éléments que nécessaire à votre liste. Vous pouvez choisir d'afficher les réponses sous forme de bouton radio à l'aide de la case à cocher prévue à cet effet.</p><list>
		  
		    <item><p>La liste déroulante « Préremplir », permet de pré-remplir de 3 façons différentes le champ :</p><list>
				  <item><p>« Texte », qui en cliquant sur le bouton « appliquer » vous permettra de saisir une valeur fixe qui sera affichée indifféremment à tous les répondants.</p></item>
				  <item><p>« Champ utilisateur » qui permet de sélectionner une des caractéristiques du profil de l'utilisateur (nom, adresse téléphone...) pour alimenter le champs</p></item>
				  <item><p>« Formule Python » qui permet d'écrire une requête Python pour aller récupérer une information pouvant pré-remplir le champ.</p></item>
				</list>
				</item>
			  <item><p>Source de données externe : permet d'aller interroger un référentiel distant pour en extraire une information destinée à alimenter le formulaire. La source de données peut être de type webservice formattée en JSON.</p></item>
		  
		  </list></item>
		  <item><p>Liste à choix multiples : identique à liste mais permet à l'utilisateur de choisir plusieurs réponses. Il est possible de limiter le nombre de choix avec l'option <em>Nombre maximal de choix</em>, 0 signifiant qu'il n'y a aucune limitation.</p></item>
		  <item><p>Tableau : Ce champ permet de mettre en place un tableau dans le formulaire. Lorsqu'on le paramètre on choisit le nombre de colonnes et de lignes qu'il va compter et les libellés choisis apparaîtront au-dessus des colonnes (pour les éléments saisis sous « Colonne ») et devant les lignes (pour les éléments saisis sous « Lignes »).</p></item>
		  <item><p>Tableau de listes : identique au type « Tableau » mais le contenu de chaque cellule est une liste définie dans l'option « Éléments » du champs.</p></item>
		  <item><p>Tableau de longueur libre : identique au type « Tableau » mais chaque fois qu'une ligne est remplie par l'utilisateur, il peut compléter une nouvelle ligne, le nombre de ces dernières n'est pas limité. Ce type offre par ailleurs la possibilité de faire le total des colonnes en cochant l'option « Ajouter une ligne de total ».</p></item>
		  <item><p>Éléments classés : Affiche à l'usager une liste de propositions précédées d'une zone de saisie permettant d'indiquer par un chiffre le classement qu'il souhaite donner à chacune d'entre elles. </p></item>
		</list>
                
            </section>
            
            <section id="champ_affiche_info">
                <title>Champs affichant une information</title>
                
                <p>Ces champs permettent la mise en forme du formulaire.</p>
                
             	<list>
             	  <item><p>Titre : permet d'insérer un titre dans le formulaire. La forme de son affichage dépend de la feuille de style du thème. Il est possible de spécifier une classe de la feuille de style dans le champs prévu à cet effet.</p></item>
             	  <item><p>Sous-titre : permet d'insérer un sous-titre dans le formulaire. La forme de son affichage dépend de la feuille de style du thème. Il est possible de spécifier une classe de la feuille de style dans le champs prévu à cet effet.</p></item>
             	  <item><p>Commentaire : permet d'insérer un commentaire dans le formulaire. La forme de son affichage dépend de la feuille de style du thème. Il est possible de spécifier une classe de la feuille de style dans le champs prévu à cet effet.</p></item>
             	  <item><p>Nouvelle page : permet de spécifier qu'une nouvelle page doit commencer à cet endroit. Le champ nouvelle page permet donc de créer des <link xref="form_multipage">formulaires multi-pages</link>.  Le champ nouvelle page permet de gérer la notion d'affichage conditionnel : certaines pages ne seront affichées que lorsqu'une réponse particulières est donnée (cf. les <link xref="form_conditionnel">formulaires conditionnels</link>). À noter qu'un formulaire multi-pages doit obligatoirement commencer par un champ de ce type. Le tite des pages sera affiché à l'utilisateur à côté du formulaire, lui permettant d'avoir une vue d'ensemble et de savoir sur quelle page il se trouve.</p></item>
             	</list>
		
                <note>
                  <title>Différences entre champs « Commentaire » et option « Remarque »</title>
                  <p>Il est généralement déconseillé d'utiliser le champs « Commentaire » pour donner des indications sur la façon de remplir un champ particulier : il est préférable d'utiliser l'option « Remarque » du champ en question pour ce faire, le texte sera alors plus explicitement lié à ce dernier.</p>
                </note>
                
            </section>
            
    </section>

</page>
