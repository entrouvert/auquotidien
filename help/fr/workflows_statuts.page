<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="workflows_statuts">


<info>
	<title type="sort">5</title>
	<link type="guide" xref="index#workflows" />
	  <desc>Gérer les étapes d'un workflow</desc>
</info>
      
<title>Les statuts de workflow</title>

<section id="general">
	<title>Généralités</title>


<p>Un workflow est un ensemble d'étapes, les statuts, par lesquelles passe le formulaire. Chaque statut regroupe une série d'<link xref="workflows_actions">actions</link> permettant d'interragir avec la demande, d'entrer en contact avec l'usager et avancer dans les étapes de traitement.</p>
</section>

<section id="creer">
	<title>Créer un statut</title>
	<p>Pour créer un nouveau statut lorsqu'on se trouve sur un workflow donné, il faut lui donner un nom et cliquer sur valider.</p>

	<figure>
			<media type="image" src="figures/creer_statut.png" />
	</figure>

</section>

<section id="ajouter">
	<title>Ajouter des actions à un statut</title>
	<p>Un statut doit être alimenté avec des <link xref="workflows_actions">actions</link> de différents types, ce sont ces actions mises bout à bout qui vont définir ce qui se passe lorsque une demande est dans un statut donné.</p>
	<p>Vous éditez un statut en cliquant sur son nom dans la liste ou sur le schéma, vous pouvez alors lui ajouter les actions qui se trouvent dans la liste déroulante à droite.</p>

	<figure>
			<media type="image" src="figures/liste_actions_workflow.png" />
	</figure>

</section>

<section id="supprimer">
	<title>Supprimer un statut</title>
	<p>Pour supprimer un statut, il suffit de cliquer sur le lien « Supprimer ». Toutes les actions qu'il contenait sont alors effacées.</p>

	<figure>
			<media type="image" src="figures/supprimer_statut.png" />
	</figure>

</section>

<section id="Options">
	<title>Options des statuts</title>
	<p>Tous les statuts dispose des mêmes options :</p>

	<figure>
			<media type="image" src="figures/creer_workflow.png" />
	</figure>	
	<p>Ces options permettent de :</p>
	<list>
		<item><p>Changer le nom du statut. Il faut noter que ce nom apparaît pour le citoyen d'une part et pour le gestionnaire du formulaire (le destinataire) d'autre part.</p></item>
		<item><p>Changer la visibilité du statut. Cela permet de faire en sorte qu'un statut soit « privé », c'est à dire qu'il soit réservé au backoffice (au destinataire) et n'apparaissent pas au demandeur.</p></item>
		<item><p>Changer le caractère final du statut. Par défaut, un statut a un caractère final s'il ne peut déboucher sur aucun autre statut. Il est parfois nécessaire de dire qu'un statut est final même si il peut déboucher sur d'autres statuts. C'est possible grâce à cette option.</p></item>
	</list>

</section>

</page>
