#! /usr/bin/env python3

import os
import subprocess
import sys

from distutils.cmd import Command
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from setuptools.command.install_lib import install_lib as _install_lib
from setuptools import setup


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


def data_tree(destdir, sourcedir):
    extensions = ['.css', '.png', '.jpeg', '.jpg', '.xml', '.html', '.js', '.ezt', '.gif', '.otf']
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in extensions]
        r.append((root.replace(sourcedir, destdir, 1), l))
        if 'CVS' in dirs:
            dirs.remove('CVS')
        if '.svn' in dirs:
            dirs.remove('.svn')
    return r


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management import call_command

            for path, dirs, files in os.walk('auquotidien'):
                if 'locale' not in dirs:
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                call_command('compilemessages')
                os.chdir(curdir)
        except ImportError:
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations\n')


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


class eo_sdist(sdist):
    def run(self):
        print("creating VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print("removing VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')


setup(
    name='wcs-au-quotidien',
    version=get_version(),
    maintainer='Frederic Peters',
    maintainer_email='fpeters@entrouvert.com',
    package_dir={'auquotidien': 'auquotidien'},
    packages=['auquotidien', 'auquotidien.modules'],
    cmdclass={
        'build': build,
        'compile_translations': compile_translations,
        'install_lib': install_lib,
        'sdist': eo_sdist,
    },
    include_package_data=True,
    data_files=data_tree('share/auquotidien/apache-errors', 'apache-errors')
    + [
        (
            'share/wcs/',
            [
                'au-quotidien-wcs-settings.xml',
            ],
        )
    ],
)
